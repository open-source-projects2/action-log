using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ActionLog.Module.LogAttributes
{
    public class AppActionLogTypeAttribute: ActionFilterAttribute
    {

        private readonly string _enumValue;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppActionLogTypeAttribute"/> class.
        /// </summary>
        /// <param name="actionLog">Object.</param>
        public AppActionLogTypeAttribute(object actionLog)
        {
            _enumValue = actionLog.ToString();
        }

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            OnActionExecuting(context);
            context.HttpContext.Items["actionLogTypeEnum"] = _enumValue;
            var guid = Guid.NewGuid();
            context.HttpContext.Items["actionLogTypeGuid"] = guid;

            await next();
        }
    }
}
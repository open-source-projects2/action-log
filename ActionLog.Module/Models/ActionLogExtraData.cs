using System;
using System.ComponentModel.DataAnnotations;

namespace ActionLog.Module.Models
{
    public class ActionLogExtraData
    {
        [Key]
        public int Id { get; set; }
        
        public Guid GroupGuid { get; set; }
        
        public string Value { get; set; }
        
        /// <summary>
        /// Дата создания записи
        /// </summary>
        public DateTime DateCreate { get; set; } = DateTime.Now;
    }
}
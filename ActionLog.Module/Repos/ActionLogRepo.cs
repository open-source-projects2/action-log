using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ActionLog.Module.Repos
{
    public class ActionLogRepo<TContext> : IActionLogRepo where TContext : DbContext
    {
        private readonly DbSet<Module.Models.ActionLog> _repo;

        public ActionLogRepo(TContext context)
        {
            _repo = context.Set<Module.Models.ActionLog>();
        }

        public async Task<object> Get()
        {
            var result = await _repo.ToListAsync();
            return result;
        }

        public async Task<IEnumerable<Module.Models.ActionLog>> Select(Expression<Func<Module.Models.ActionLog, bool>> expression)
        {
            return await _repo.Where(expression).ToListAsync();
        }

        public IQueryable<Module.Models.ActionLog> SelectQuery(Expression<Func<Module.Models.ActionLog, bool>> expression)
        {
            return _repo.Where(expression);
        }

        public (Task<int>, IQueryable<Module.Models.ActionLog>) SelectQueryPagination(
            Expression<Func<Module.Models.ActionLog, bool>> expression, int start = 1, int rowCount = 100)
        {
            var count = _repo.Where(expression).CountAsync();
            var result = _repo.Where(expression).Skip(start).Take(rowCount);

            return (count, result);
        }
    }
}
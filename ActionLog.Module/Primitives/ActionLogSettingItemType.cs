namespace ActionLog.Module.Primitives
{
    public enum ActionLogSettingItemType
    {
        Text,
        LongText,
        Number,
        Checkbox,
        Radio,
        Array,
        Json,
        Enum,
        DateTime
    }
}
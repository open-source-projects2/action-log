namespace ActionLog.Module.Dtos
{
    public class ActionLogResolverTableKeyValueDto
    {
        public string Key { get; set; }
        
        public string Value { get; set; }
    }
}
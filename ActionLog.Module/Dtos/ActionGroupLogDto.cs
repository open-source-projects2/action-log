using System;
using System.Collections.Generic;
using System.Linq;

namespace ActionLog.Module.Dtos
{
    public class ActionGroupLogDto
    {
        /// <summary>
        /// Gets or sets group Id
        /// </summary>
        public Guid GroupId { get; set; }

        /// <summary>
        /// Gets or sets userId
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Gets or sets userName
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets userEmail
        /// </summary>
        public string UserEmail { get; set; }

        /// <summary>
        /// Gets or sets actionId
        /// </summary>
        public Guid? ActionId { get; set; }

        /// <summary>
        /// Gets or sets action
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Gets or sets modificationDate
        /// </summary>
        public DateTime ModificationDate { get; set; }
        
        public string ExtraData { get; set; }

        /// <summary>
        /// Gets or sets Modifications
        /// </summary>
        public IEnumerable<ActionLogDto> Modifications { get; set; }

        /// <summary>
        /// Gets modification count
        /// </summary>
        public int ModificationCount => Modifications.Count();
    }
}
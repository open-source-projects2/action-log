using System;
using Microsoft.EntityFrameworkCore;

namespace ActionLog.Module.Dtos
{
    public class ActionLogDto
    {
        public int Id { get; set; }

        public Guid GroupGuid { get; set; }

        public string ObjectName { get; set; }
        
        public string ObjectDescription { get; set; }

        public string ObjectFullName { get; set; }

        public string ObjectId { get; set; }

        public EntityState State { get; set; }

        public string StateName => State.ToString();

        public string Column { get; set; }

        public ActionLogClassAttributeInfoDto ColumnInfo { get; set; }

        public string OldValue { get; set; }

        public string NewValue { get; set; }

        public DateTime DateCreate { get; set; }
    }
}
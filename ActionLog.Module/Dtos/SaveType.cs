using System.Threading.Tasks;

namespace ActionLog.Module.Dtos
{
    public class SaveType
    {
        public Task<int> TaskIntValue { get; set; }

        public int IntValue { get; set; }
    }
}
using System.Threading.Tasks;

namespace ActionLog.Module.Logics
{
    public interface IActionLogResolver
    {
        
        /// <summary>
        /// Получение значения для вывода в логах
        /// </summary>
        /// <param name="objectId"></param>
        /// <returns></returns>
        Task<string> ResolveTitleAsync(string objectId);
        string ResolveTitle(string objectId);
    }
}
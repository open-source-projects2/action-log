#nullable enable
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using ActionLog.Module.Dtos;
using ActionLog.Module.Dtos.Grouped;
using ActionLog.Module.LogAttributes;
using ActionLog.Module.Models;
using ActionLog.Module.Primitives;
using ActionLog.Module.Repos;
using HardCode.ICacheService;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace ActionLog.Module.Logics.Impl
{
    public class
        ActionLogViewLogic<TContext, TUser, TKey, TOutActionGroupLogDto, TCacheService> : IActionLogViewLogic<
            TOutActionGroupLogDto, TUser, TKey>
        where TContext : DbContext
        where TKey : IEquatable<TKey>
        where TUser : IdentityUser<TKey>
        where TOutActionGroupLogDto : ActionGroupLogDto, new()
        where TCacheService : ICacheService
    {
        private readonly IActionLogRepo _journalRepo;
        private readonly IActionLogTrackedClassRepo<TContext> _trackedClassRepo;
        private readonly TContext _dbContext;

        private readonly TCacheService _cacheService;
        private readonly DbSet<TUser> _users;

        public ActionLogViewLogic(
            IActionLogRepo journalRepo,
            IActionLogTrackedClassRepo<TContext> trackedClassRepo,
            TContext context,
            TCacheService cacheService)
        {
            _journalRepo = journalRepo;
            _trackedClassRepo = trackedClassRepo;
            _cacheService = cacheService;
            _dbContext = context;
            _users = context.Set<TUser>();
        }

        /// <inheritdoc/>
        public async Task<ActionLogSearhResultDto<TOutActionGroupLogDto>> Get(ActionLogQuery filter)
        {
            var users = await GetUsers();
            return await Search(filter, users);
        }

        public async Task<ActionLogSearhResultDto<TOutActionGroupLogDto>> Get(ActionLogQuery filter,
            Expression<Func<TUser, ActionLogUserDto<TKey>>> userQuery)
        {
            var users = await GetUsers(userQuery);
            return await Search(filter, users);
        }
        
        public async Task<ActionLogSearhResultDto<TOutActionGroupLogDto>> Get(ActionLogQuery filter, List<ActionLogUserDto<TKey>> users)
        {
            return await Search(filter, users);
        }

        /// <summary>
        /// Получение информации по ID действия
        /// </summary>
        /// <param name="actionLogTypeId"></param>
        /// <param name="userQuery"></param>
        /// <returns></returns>
        public async Task<ActionLogGroupTypeDto> GetGrouped(Guid actionLogTypeId,
            Expression<Func<TUser, ActionLogUserDto<TKey>>> userQuery)
        {
            var users = await GetUsers(userQuery);
            return await SearchGroupedLog<ActionLogGroupTypeDto>(actionLogTypeId, users);
        }
        
        public async Task<ActionLogGroupTypeDto> GetGrouped(Guid actionLogTypeId,List<ActionLogUserDto<TKey>> users)
        {
            return await SearchGroupedLog<ActionLogGroupTypeDto>(actionLogTypeId, users);
        }

        public async Task<T> GetGrouped<T>(Guid actionLogTypeId,
            Expression<Func<TUser, ActionLogUserDto<TKey>>> userQuery)
            where T : ActionLogGroupTypeDto, new ()
        {
            var users = await GetUsers(userQuery);
            return await SearchGroupedLog<T>(actionLogTypeId, users);
        }
        
        public async Task<T> GetGrouped<T>(Guid actionLogTypeId, List<ActionLogUserDto<TKey>> users)
            where T : ActionLogGroupTypeDto, new ()
        {
            return await SearchGroupedLog<T>(actionLogTypeId, users);
        }
        
        public async Task<ActionLogGroupTypeDto> GetGrouped(Guid actionLogTypeId) 
        {
            var users = await GetUsers();
            return await SearchGroupedLog<ActionLogGroupTypeDto>(actionLogTypeId, users);
        }
        
        public async Task<T> GetGrouped<T>(Guid actionLogTypeId)
            where T : ActionLogGroupTypeDto, new ()
        {
            var users = await GetUsers();
            return await SearchGroupedLog<T>(actionLogTypeId, users);
        }
        
        /// <summary>
        /// Получение списка изменений с группировкой по действию
        /// </summary>
        /// <returns></returns>
        public async Task<ActionLogSearhResultDto<T>> GroupList<T>(ActionLogGroupListQueryDto filter,
            Expression<Func<TUser, ActionLogUserDto<TKey>>> userQuery)
            where T : ActionLogGroupListItemDto, new()
        {
            var start = (filter.Page - 1) * filter.RowCount;

            var users = await GetUsers(userQuery);

            var userId = filter.UserId?.ToString();
            var query = _journalRepo
                .SelectQuery(x =>
                    filter.ActionLogTypes == null || filter.ActionLogTypes.Count == 0 ||
                    filter.ActionLogTypes.Contains(x.ActionLogTypeEnum))
                .Where(x => filter.StartDate == null || x.DateCreate >= filter.StartDate.Value.Date)
                .Where(x => filter.EndDate == null || x.DateCreate <= filter.EndDate.Value.Date)
                .Where(x => userId == null || x.UserId == userId)
                .Where(x => string.IsNullOrEmpty(filter.ObjectFullName) || x.UserId == filter.ObjectFullName);

            var data =  (await query.AsNoTracking()
                    .ToListAsync())
                    .GroupBy(x => new
                    {
                        x.ActionLogTypeEnum,
                        x.ActionLogTypeGroupId,
                        x.UserId
                    })
                .Select(x =>
                    {
                        var user = users.FirstOrDefault(u => u.Id.ToString() == x.Key.UserId);

                        return new T
                        {
                            Action = x.Key.ActionLogTypeEnum,
                            ActionId = x.Key.ActionLogTypeGroupId,
                            UserId = x.Key.UserId,
                            UserEmail = user?.Email,
                            UserName = user?.UserName,
                            ModificationCount = x.Count(),
                            ModificationDate = x.Min(m => m.DateCreate),
                            Items = x.Select(a => GetClassDescription(a.ObjectFullName))
                        };
                    })
                .Skip(start)
                .Take(filter.RowCount)
                .OrderByDescending(x => x.ModificationDate)
                .ToList();
            
            return new ActionLogSearhResultDto<T>
            {
                Total = await query.GroupBy(x => new
                {
                    x.ActionLogTypeEnum,
                    x.ActionLogTypeGroupId,
                    x.UserId
                }).Select(s => s.Key.ActionLogTypeGroupId).CountAsync(),
                CurrentPage = filter.Page,
                RowCount = data.Count(),
                Items = data,
            };
        }
        

        private async Task<List<ActionLogUserDto<TKey>>> GetUsers()
        {
            var key = "actionLogUsersBase";
            var ret = _cacheService.Get<List<ActionLogUserDto<TKey>>>(key);
            if (ret != null) return ret;

            ret = await _users.AsNoTracking()
                .Select(x => new ActionLogUserDto<TKey>
                {
                    Id = x.Id,
                    Email = x.Email,
                    UserName = x.UserName,
                })
                .ToListAsync();
            _cacheService.Store(key, ret);
            return ret;
        }

        private async Task<List<ActionLogUserDto<TKey>>> GetUsers(Expression<Func<TUser, ActionLogUserDto<TKey>>> query)
        {
            var key = "actionLogUsers";
            var ret = _cacheService.Get<List<ActionLogUserDto<TKey>>>(key);
            if (ret != null) return ret;

            ret = await _users.AsNoTracking()
                .Select(query)
                .ToListAsync();
            _cacheService.Store(key, ret);
            return ret;
        }

        private async Task<T> SearchGroupedLog<T>(Guid actionLogTypeId, List<ActionLogUserDto<TKey>> users)
            where T : ActionLogGroupTypeDto, new ()
        {
            var data = await _journalRepo
                .SelectQuery(x => x.ActionLogTypeGroupId == actionLogTypeId)
                .AsNoTracking()
                .Select(a => new
                {
                    a.Id,
                    a.GroupGuid,
                    a.UserId,
                    a.DateCreate,
                    a.ObjectName,
                    a.ObjectFullName,
                    a.ObjectId,
                    a.State,
                    a.ActionLogTypeEnum,
                    a.ActionLogTypeGroupId,
                    a.Column,
                    a.OldValue,
                    a.NewValue,
                    ExtraData = _dbContext.Set<ActionLogExtraData>()
                        .AsNoTracking()
                        .FirstOrDefault(x => x.GroupGuid == a.ActionLogTypeGroupId).Value
                })
                .ToListAsync();

            var grouped = data
                .GroupBy(x => new
                {
                    x.ExtraData,
                    x.UserId,
                    ActionId = x.ActionLogTypeGroupId,
                    Action = x.ActionLogTypeEnum,
                })
                .Select(g =>
                {
                    var user = users.FirstOrDefault(u => u.Id.ToString() == g.Key.UserId);
                    return new T
                    {
                        UserId = g.Key.UserId,
                        UserEmail = user?.Email,
                        UserName = user?.UserName,
                        ActionId = g.Key.ActionId,
                        Action = g.Key.Action,
                        ExtraData = g.Key.ExtraData,
                        ModificationDate = g.Max(a => a.DateCreate),
                        Entities = g
                            .GroupBy(a => new
                            {
                                a.ObjectFullName
                            })
                            .Select(a => new ActionLogGroupTypeEntityDto
                            {
                                ObjectFullName = a.Key.ObjectFullName,
                                ObjectDescription = GetClassDescription(a.Key.ObjectFullName),
                                Objects = a.GroupBy(x => x.ObjectId)
                                    .Select(u => new ActionLogGroupTypeEntityObjectDto
                                    {
                                        ObjectId = u.Key,
                                        Modifications = u.Select(s =>
                                        {
                                            var classInfo = GetClassColumnInfo(a.Key.ObjectFullName, s.Column);
                                            return new ActionLogDto
                                            {
                                                Id = s.Id,
                                                GroupGuid = s.GroupGuid,
                                                ObjectDescription = GetClassDescription(a.Key.ObjectFullName),
                                                ObjectName = s.ObjectName,
                                                ObjectFullName = s.ObjectFullName,
                                                ObjectId = s.ObjectId,
                                                State = s.State,
                                                DateCreate = s.DateCreate.ToLocalTime(),
                                                Column = s.Column,
                                                ColumnInfo = classInfo,
                                                OldValue = classInfo?.Resolver == null
                                                    ? s.OldValue
                                                    : classInfo?.Resolver.ResolveTitle(s.OldValue),
                                                NewValue = classInfo?.Resolver == null
                                                    ? s.NewValue
                                                    : classInfo.Resolver.ResolveTitle(s.NewValue),
                                            };
                                        })
                                    })
                            })
                    };
                })
                .FirstOrDefault();
            return grouped;
        }

        private async Task<ActionLogSearhResultDto<TOutActionGroupLogDto>> Search(ActionLogQuery filter,
            List<ActionLogUserDto<TKey>> users)
        {
            var query = Queryable(filter);
            var start = (filter.Page - 1) * filter.RowCount;

            var entities = (await query
                    .Select(a => new
                    {
                        a.Id,
                        a.GroupGuid,
                        a.UserId,
                        a.DateCreate,
                        a.ObjectName,
                        a.ObjectFullName,
                        a.ObjectId,
                        a.State,
                        a.ActionLogTypeEnum,
                        a.ActionLogTypeGroupId,
                        a.Column,
                        a.OldValue,
                        a.NewValue,
                        ExtraData = _dbContext.Set<ActionLogExtraData>()
                            .AsNoTracking()
                            .FirstOrDefault(x => x.GroupGuid == a.ActionLogTypeGroupId).Value
                    })
                    .AsNoTracking()
                    .Skip(start)
                    .Take(filter.RowCount)
                    .OrderByDescending(x => x.DateCreate)
                    .ToListAsync())
                .GroupBy(x => new
                {
                    GroupId = x.ActionLogTypeGroupId ?? x.GroupGuid,
                    x.ExtraData,
                    x.GroupGuid,
                    x.UserId,
                    ActionId = x.ActionLogTypeGroupId,
                    Action = x.ActionLogTypeEnum,
                })
                .Select(g => new
                {
                    g.Key.GroupId,
                    g.Key.UserId,
                    DateCreate = g.Min(x => x.DateCreate),
                    g.Key.ActionId,
                    g.Key.Action,
                    g.Key.ExtraData,
                    Modifications = g.Select(x =>
                    {
                        var user = users.FirstOrDefault(u => u.Id.ToString() == x.UserId);
                        return new
                        {
                            x.Id,
                            x.GroupGuid,
                            x.UserId,
                            user?.UserName,
                            UserEmail = user?.Email,
                            x.DateCreate,
                            x.ObjectName,
                            x.ObjectFullName,
                            x.ObjectId,
                            x.State,
                            x.ActionLogTypeEnum,
                            x.ActionLogTypeGroupId,
                            x.Column,
                            x.OldValue,
                            x.NewValue,
                        };
                    }),
                })
                .OrderByDescending(x => x.DateCreate)
                .ToList();
            var count = entities.Count;

            var classAttributeInfo = GetClassAttributeInfoDto(filter.ObjectFullName);
            return new ActionLogSearhResultDto<TOutActionGroupLogDto>
            {
                Total = count,
                CurrentPage = filter.Page,
                RowCount = filter.RowCount,
                Items = count > 0
                    ? entities.Select(g => new TOutActionGroupLogDto()
                    {
                        GroupId = g.GroupId,
                        UserId = g.UserId,
                        UserName = g.Modifications.FirstOrDefault()?.UserName,
                        UserEmail = g.Modifications.FirstOrDefault()?.UserEmail,
                        ActionId = g.ActionId,
                        Action = g.Action,
                        ExtraData = g.ExtraData,
                        ModificationDate = g.DateCreate.ToLocalTime(),
                        Modifications = g.Modifications.Select(x =>
                        {
                            var classInfo = classAttributeInfo.FirstOrDefault(i => i.ColumnName == x.Column);
                            return new ActionLogDto
                            {
                                Id = x.Id,
                                GroupGuid = x.GroupGuid,
                                ObjectDescription = GetClassDescription(filter.ObjectFullName),
                                ObjectName = x.ObjectName,
                                ObjectFullName = x.ObjectFullName,
                                ObjectId = x.ObjectId,
                                State = x.State,
                                DateCreate = x.DateCreate.ToLocalTime(),
                                Column = x.Column,
                                ColumnInfo = classInfo,
                                OldValue = classInfo?.Resolver == null
                                    ? x.OldValue
                                    : classInfo.Resolver.ResolveTitle(x.OldValue),
                                NewValue = classInfo?.Resolver == null
                                    ? x.NewValue
                                    : classInfo.Resolver.ResolveTitle(x.NewValue),
                            };
                        }).OrderBy(x => x.GroupGuid).ThenBy(x => x.ObjectName).ThenBy(x => x.DateCreate),
                    })
                    : null,
            };
        }

        private IQueryable<Models.ActionLog> Queryable(ActionLogQuery filter)
        {
            return _journalRepo.SelectQuery(q =>
                (filter.Column == null || q.Column == filter.Column) &&
                (filter.ActionLogTypes == null || !filter.ActionLogTypes.Any() ||
                 filter.ActionLogTypes.Contains(q.ActionLogTypeEnum)) &&
                (filter.UserId == null || q.UserId == filter.UserId) &&
                (filter.Object == null || q.ObjectName == filter.Object) &&
                (filter.ObjectFullName == null || q.ObjectFullName == filter.ObjectFullName) &&
                (string.IsNullOrEmpty(filter.ObjectId) || q.ObjectId == filter.ObjectId) &&
                (filter.OldValue == null || q.OldValue == filter.OldValue) &&
                (filter.NewValue == null || q.NewValue == filter.NewValue) &&
                (filter.State == null || q.State == filter.State) &&
                (filter.StartDate == null || q.DateCreate >= filter.StartDate) &&
                (filter.EndDate == null || q.DateCreate <= filter.EndDate)
            );
        }

        private static string GetDescription(Enum genericEnum)
        {
            var genericEnumType = genericEnum.GetType();
            var memberInfo = genericEnumType.GetMember(genericEnum.ToString());

            if (memberInfo.Length > 0)
            {
                var attribs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attribs.Any())
                {
                    return ((DescriptionAttribute) attribs.ElementAt(0)).Description;
                }
            }

            return genericEnum.ToString();
        }

        private string GetClassQualifiedName(string classFullName)
        {
            var key = "allTrackedClass";
            var ret = _cacheService.Get<IEnumerable<ActionLogTrackedClass>>(key);
            if (ret == null)
            {
                ret = _trackedClassRepo.Get().Result;
                _cacheService.Store(key, ret);
            }

            var item = ret.FirstOrDefault(x => x.ClassFullName == classFullName);
            return item?.AssemblyQualifiedName;
        }


        private ActionLogClassAttributeInfoExtraDto GetClassColumnInfo(string classFullName, string columnName)
        {
            var classInfo = GetClassAttributeInfoDto(classFullName);
            return classInfo.FirstOrDefault(x => x.ColumnName == columnName);
        }

        private IEnumerable<ActionLogClassAttributeInfoExtraDto> GetClassAttributeInfoDto(string classFullName)
        {
            var key = "atributeInfo" + classFullName;
            var result = new List<ActionLogClassAttributeInfoExtraDto>();
            var classQualifiedName = GetClassQualifiedName(classFullName);
            if (string.IsNullOrEmpty(classQualifiedName))
            {
                return new List<ActionLogClassAttributeInfoExtraDto>();
            }

            Console.WriteLine("classQualifiedName=" + classQualifiedName);
            var type = Type.GetType(classQualifiedName);
            if (type == null)
            {
                return new List<ActionLogClassAttributeInfoExtraDto>();
            }

            var properties = type.GetProperties();
            foreach (var property in properties)
            {
                var hasForeignKey = property.CustomAttributes.Any(attribute =>
                    attribute.AttributeType.Name == "ForeignKeyAttribute");

                if (hasForeignKey || property.GetGetMethod()!.IsVirtual)
                {
                    continue;
                }

                var itemKey = key + property.Name;
                var item = _cacheService.Get<ActionLogClassAttributeInfoDto>(itemKey);
                IActionLogResolver? resolverObj = null;
                if (item == null)
                {
                    item = new ActionLogClassAttributeInfoDto
                    {
                        ColumnName = property.Name,
                        ColumnType = GetPropertySettingItemType(property),
                        EnumValues = new List<ActionLogBaseEnumValuesDto>()
                    };
                    try
                    {
                        item.ColumnDisplayName = property.GetCustomAttribute<DisplayNameAttribute>()?.DisplayName;
                        if (property.PropertyType.IsEnum)
                        {
                            item.ColumnType = ActionLogSettingItemType.Enum;
                            item.EnumValues = GetEnumInfo(property);
                        }
                    }
                    catch (Exception ex)
                    {
                        item.ColumnDisplayName = property.Name;
                    }

                    _cacheService.Store(itemKey, item);
                }

                try
                {
                    var resolver = property.GetCustomAttribute<ActionLogResolverAttribute>()?.GetResolver();
                    if (resolver != null)
                    {
                        resolverObj = (IActionLogResolver) Activator.CreateInstance(resolver,
                            new object?[] {_dbContext, _cacheService});
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }


                result.Add(new ActionLogClassAttributeInfoExtraDto
                {
                    ColumnName = item.ColumnName,
                    ColumnType = item.ColumnType,
                    EnumValues = item.EnumValues,
                    ColumnDisplayName = item.ColumnDisplayName,
                    Resolver = resolverObj,
                });
            }

            _cacheService.Store(key, result, 300);
            return result;
        }

        private string GetClassDescription(string classFullName)
        {
            var classQualifiedName = GetClassQualifiedName(classFullName);
            if (string.IsNullOrEmpty(classQualifiedName))
            {
                return classFullName;
            }
            
            var type = Type.GetType(classQualifiedName);
            if (type == null)
            {
                return classFullName;
            }

            var descriptions = (DescriptionAttribute[])
                type.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return descriptions.Length == 0 ? type.Name : descriptions[0].Description;
        }

        private static ActionLogSettingItemType GetPropertySettingItemType(PropertyInfo property)
        {
            if (property.PropertyType == typeof(string))
            {
                return ActionLogSettingItemType.Text;
            }

            if (property.PropertyType == typeof(bool))
            {
                return ActionLogSettingItemType.Checkbox;
            }

            if (property.PropertyType == typeof(int) ||
                property.PropertyType == typeof(int?) ||
                property.PropertyType == typeof(double) ||
                property.PropertyType == typeof(double))
            {
                return ActionLogSettingItemType.Number;
            }

            if (typeof(IEnumerable).IsAssignableFrom(property.PropertyType) ||
                typeof(IList).IsAssignableFrom(property.PropertyType) ||
                typeof(ICollection).IsAssignableFrom(property.PropertyType))
            {
                return ActionLogSettingItemType.Array;
            }

            if (property.PropertyType == typeof(DateTime) || property.PropertyType == typeof(DateTime?))
            {
                return ActionLogSettingItemType.DateTime;
            }

            return ActionLogSettingItemType.Json;
        }

        private static IEnumerable<ActionLogBaseEnumValuesDto> GetEnumInfo(PropertyInfo propertyInfo)
        {
            return (from Enum e in Enum.GetValues(propertyInfo.PropertyType)
                select new ActionLogBaseEnumValuesDto
                {
                    Description = GetDescription(e), Value = e, StringValue = e.ToString(),
                }).ToList();
        }

        public void SetActionLogType(string type)
        {
            throw new NotImplementedException();
        }

        public void SetActionLogCurrentUserId(string type)
        {
            throw new NotImplementedException();
        }
    }
}
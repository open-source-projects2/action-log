namespace ActionLog.Module.Logics
{
    public interface IActionLogHttpContextLogic
    {
        /// <summary>
        /// установка значения ActionLogType
        /// </summary>
        /// <param name="type"></param>
        void SetActionLogType(string type);

        /// <summary>
        /// Получение текущего значения ActionLogType
        /// </summary>
        /// <returns></returns>
        string GetActionLogType();
        
        /// <summary>
        /// Установка ID текущего пользователя
        /// </summary>
        /// <param name="userId"></param>
        void SetActionLogCurrentUserId(string userId);
        
        /// <summary>
        /// Получение ID текущего пользователя
        /// </summary>
        /// <returns></returns>
        string GetActionLogCurrentUserId();
        
        /// <summary>
        /// Сохранение расширенных данных
        /// </summary>
        /// <param name="data"></param>
        void SetExtraData(object data);

        /// <summary>
        /// Получение текущего значения ExtraData
        /// </summary>
        T GetExtraData<T>() where T : class;
    }
}
using System;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace ActionLog.Module.Logics
{
    public interface IActionLogBaseLogic
    {
        void DetectContextChanges(EntityEntry change, Guid groupGuid);

        void NewObject(EntityEntry change, Guid groupGuid);
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using ActionLog.Module.Models;

namespace ActionLog.Module.Logics
{
    public interface IActionLogSettingLogic
    {
        /// <summary>
        /// Добавление/удаление сущности из списка отслеживаемых классов
        /// </summary>
        /// <param name="isTracked">Bool tracked class.</param>
        /// <typeparam name="T">Class.</typeparam>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task TrackedClass<T>(bool isTracked);

        /// <summary>
        /// Получение списка отслеживаемых классов
        /// </summary>
        /// <returns>A <see cref="Task"/> representing the asynchronous operation.</returns>
        Task<IEnumerable<ActionLogTrackedClass>> Get();
    }
}
using System;
using System.Linq.Expressions;
using AcltionLog.Api.Data;
using AcltionLog.Api.Data.Models;
using ActionLog.Module.Dtos;
using ActionLog.Module.Resolvers;
using HardCode.ICacheService;

namespace AcltionLog.Api.ActionLogResolver
{
    public class ActionLogRoleIdResolver : ActionLogBaseResolver<ApplicationDbContext, Role>
    {
        public ActionLogRoleIdResolver(
            ApplicationDbContext context,
            ICacheService cacheService)
            : base(context, cacheService)
        {
        }

        protected override Expression<Func<Role, ActionLogResolverTableKeyValueDto>> SelectExpression()
        {
            return x => new ActionLogResolverTableKeyValueDto
            {
                Key = x.Id.ToString(),
                Value = x.Name,
            };
        }
    }
}
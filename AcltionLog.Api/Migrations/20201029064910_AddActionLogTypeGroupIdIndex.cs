﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AcltionLog.Api.Migrations
{
    public partial class AddActionLogTypeGroupIdIndex : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_ActionLogs_ActionLogTypeGroupId",
                table: "ActionLogs",
                column: "ActionLogTypeGroupId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ActionLogs_ActionLogTypeGroupId",
                table: "ActionLogs");
        }
    }
}

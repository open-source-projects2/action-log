﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AcltionLog.Api.Migrations
{
    public partial class AddRoleIdInUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RoleId",
                table: "AuthUsers",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_AuthUsers_RoleId",
                table: "AuthUsers",
                column: "RoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_AuthUsers_Roles_RoleId",
                table: "AuthUsers",
                column: "RoleId",
                principalTable: "Roles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuthUsers_Roles_RoleId",
                table: "AuthUsers");

            migrationBuilder.DropIndex(
                name: "IX_AuthUsers_RoleId",
                table: "AuthUsers");

            migrationBuilder.DropColumn(
                name: "RoleId",
                table: "AuthUsers");
        }
    }
}

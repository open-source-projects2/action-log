using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HardCode.ICacheService;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace AcltionLog.Api.Cache
{
    public class RedisCacheService : ICacheService
    {
        protected IDistributedCache Cache;

        private static int DefaultCacheDuration => 60;

        public RedisCacheService(IDistributedCache cache)
        {
            Cache = cache;
        }

        public void Store(string key, object content)
        {
            Store(key, content, DefaultCacheDuration);
        }

        public void Store(string key, object content, int duration)
        {
            string toStore;
            if (content is string)
            {
                toStore = (string) content;
            }
            else
            {
                toStore = JsonConvert.SerializeObject(content);
            }

            duration = duration <= 0 ? DefaultCacheDuration : duration;
            Cache.SetAsync(key, Encoding.UTF8.GetBytes(toStore), new DistributedCacheEntryOptions()
            {
                AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(duration)
            }).Wait();
        }

        public bool CacheClear()
        {
            throw new NotImplementedException();
        }

        public T Get<T>(string key) where T : class
        {
            var fromCache = Cache.GetAsync(key);
            fromCache.Wait();
            if (fromCache.Result == null)
            {
                return null;
            }

            var str = Encoding.UTF8.GetString(fromCache.Result);
            if (typeof(T) == typeof(string))
            {
                return str as T;
            }

            return JsonConvert.DeserializeObject<T>(str);
        }

        public string Get(string key)
        {
            throw new NotImplementedException();
        }

        public void Remove(IEnumerable<string> keys)
        {
            foreach (var key in keys)
            {
                Remove(key);
            }
        }

        public void Remove(string key)
        {
            Cache.Remove(key);
        }

        public bool Exists(string key)
        {
            return true;
        }
        
        public string GetCacheKey(string baseKey, string fullKey)
        {
            var ret = Get<IList<string>>(baseKey + "Filters") ?? new List<string>();
            var keyExist = ret.FirstOrDefault(x => x == fullKey);
            if (keyExist != null) return fullKey;
            ret.Add(fullKey);
            Store(baseKey + "Filters", ret);
            return fullKey;
        }
    }
}
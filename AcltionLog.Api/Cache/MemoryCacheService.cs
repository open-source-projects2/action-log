using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HardCode.ICacheService;
using Microsoft.Extensions.Caching.Memory;

namespace AcltionLog.Api.Cache
{
    public class MemoryCacheService : ICacheService
    {
        protected IMemoryCache Cache;

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryCacheService"/> class.
        /// </summary>
        /// <param name="cache">IMemoryCache.</param>
        public MemoryCacheService(IMemoryCache cache)
        {
            Cache = cache;
        }

        /// <inheritdoc/>
        public void Store(string key, object content)
        {
            Store(key, content, DefaultCacheDuration);
        }

        /// <inheritdoc/>
        public void Store(string key, object content, int duration)
        {
            if (Cache.TryGetValue(key, out var cached))
            {
                Cache.Remove(key);
            }

            Cache.Set(key, content,
                new MemoryCacheEntryOptions
                {
                    AbsoluteExpiration = DateTime.Now + TimeSpan.FromMinutes(duration), Priority = CacheItemPriority.Low
                });
        }

        /// <inheritdoc/>
        public bool CacheClear()
        {
            try
            {
                var prop = Cache.GetType()
                    .GetProperty("EntriesCollection", BindingFlags.Instance | BindingFlags.GetProperty | BindingFlags.NonPublic | BindingFlags.Public);
                var innerCache = prop.GetValue(Cache);
                var clearMethod = innerCache
                    .GetType()
                    .GetMethod("Clear", BindingFlags.Instance | BindingFlags.Public);
                clearMethod?.Invoke(innerCache, null);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        private static int DefaultCacheDuration => 60;

        /// <inheritdoc/>
        public T Get<T>(string key)
            where T : class
        {
            object result;
            if (Cache.TryGetValue(key, out result))
            {
                return result as T;
            }

            return null;
        }

        public string Get(string key)
        {
            throw new NotImplementedException();
        }

        public void Remove(IEnumerable<string> keys)
        {
            throw new NotImplementedException();
        }

        public void Remove(string key)
        {
            throw new NotImplementedException();
        }

        public bool Exists(string key)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public string GetCacheKey(string baseKey, string fullKey)
        {
            var ret = Get<IList<string>>(baseKey + "Filters") ?? new List<string>();
            var keyExist = ret.FirstOrDefault(x => x == fullKey);
            if (keyExist != null)
            {
                return fullKey;
            }

            ret.Add(fullKey);
            Store(baseKey + "Filters", ret);
            return fullKey;
        }
    }
}
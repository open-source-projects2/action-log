using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using AcltionLog.Api.ActionLogResolver;
using ActionLog.Module.LogAttributes;
using ActionLog.Module.Logics;
using Microsoft.AspNetCore.Identity;

namespace AcltionLog.Api.Data.Models
{
    [Description("Пользователь")]
    public class User : IdentityUser
    {
        [DisplayName("Ф.И.О")]
        public string Fio { get; set; }

        [DisplayName("Информация")]
        public string Data { get; set; }

        [ActionLogResolver(typeof(ActionLogRoleIdResolver))]
        public int RoleId { get; set; }

        [ForeignKey(nameof(RoleId))]
        public virtual Role Role { get; set; }
    }
}
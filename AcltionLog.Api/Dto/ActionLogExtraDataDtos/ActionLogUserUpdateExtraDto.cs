using System;

namespace AcltionLog.Api.Dto.ActionLogExtraDataDtos
{
    public class ActionLogUserUpdateExtraDto
    {
        public string Title { get; set; }
        
        public DateTime DateTime { get; set; }
        
        public string UserId { get; set; }
    }
}
using System;
using AcltionLog.Api.Primitives;
using AcltionLog.Api.Utils;
using ActionLog.Module.Dtos;
using ActionLog.Module.Utils;
using Newtonsoft.Json;

namespace AcltionLog.Api.Dto
{
    public class ActionGroupLogExDto : ActionGroupLogDto
    {
        private ActionLogTypeEnum ActionLogType => Enum.Parse<ActionLogTypeEnum>(Action);

        public string ActionTitle
        {
            get
            {
                if (string.IsNullOrEmpty(Action))
                {
                    return string.Empty;
                }

                return ActionLogType.GetDescription();
            }
        }

        public object ExtraDataObj
        {
            get
            {
                var type = ActionLogType.GetExtraDataType();
                return type == null ? null : JsonConvert.DeserializeObject(ExtraData, type);
            }
        }
    }
}